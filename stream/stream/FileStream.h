#pragma once
#include "OutStream.h"
#include <stdlib.h>

namespace msl {
	class FileStream : public OutStream {
	public:
		FileStream(const char* filepath);
		~FileStream();
		FileStream& operator<<(int num);
		FileStream& operator<<(const char* str);
		FileStream& operator<<(void(*pf)(FILE* f));

	};
}