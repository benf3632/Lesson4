#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

namespace msl {
	class OutStream
	{
	protected:
		FILE * _file;
	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)(FILE* f));
	};

	void endline(FILE* f);
}