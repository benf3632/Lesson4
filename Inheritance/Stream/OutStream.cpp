#include "OutStream.h"

OutStream::OutStream()
{
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(stdout, "%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(stdout, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE* f))
{
	pf(stdout);
	return *this;
}


void endline(FILE* f)
{
	fprintf(f, "\n");
}
