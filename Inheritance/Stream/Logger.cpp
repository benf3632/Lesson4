#include "Logger.h"

void Logger::setStartLine()
{
	static unsigned int count = 0;

	if (_startLine) //checks if its a new line
	{
		this->_startLine = false;
		count++;
		fprintf(stdout, "%d LOG: ", count);
	}
}


Logger::Logger() : _startLine(true)
{
}

Logger::~Logger()
{
}

Logger & operator<<(Logger & l, const char * msg)
{
	l.setStartLine();

	if (strchr(msg, '\n')) //checks if the the msg contains new line to set start line to true
	{
		l._startLine = true;
	}
	else
	{
		l._startLine = false;
	}

	l.os << msg;
	return l;
}

Logger & operator<<(Logger & l, int num)
{
	l.setStartLine(); //checks and prints if necceary
	l.os << num;
	return l;
}

Logger & operator<<(Logger & l, void(*pf)(FILE* f))
{
	l.setStartLine();
	l._startLine = true;
	pf(stdout);
	return l;
}
