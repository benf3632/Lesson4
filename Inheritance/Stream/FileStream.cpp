#include "FileStream.h"

FileStream::FileStream(const char * filepath)
{
	this->_file = fopen(filepath, "w");
	if (_file == NULL)
	{
		exit(1);
	}
}

FileStream::~FileStream()
{
	fclose(_file);
}

FileStream& FileStream::operator<<(int num)
{
	fprintf(this->_file, "%d", num);
	return *this;
}

FileStream& FileStream::operator<<(const char* str)
{
	fprintf(this->_file, "%s", str);
	return *this;
}

FileStream& FileStream::operator<<(void(*pf)(FILE* f))
{
	pf(_file);
	return *this;
}
