#include <iostream>
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"

int main(int argc, char **argv)
{
	OutStream os;
	os << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	FileStream file("..\\file.txt");
	file << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	file << "Hello";
	OutStreamEncrypted ose(3);
	ose << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	Logger l;
	Logger l1;
	l << "Hello" << endline;
	l << "Google\n";
	l1 << 5;
	l1 << " Y" << endline;


	system("PAUSE");
	return 0;
}
