#include "OutStreamEncrypted.h"

char * OutStreamEncrypted::encrypt(char * str)
{
	char* temp = nullptr;
	if (!check_str(str))
	{
		return nullptr;
	}
	char shifted = 0;
	temp = new char[strlen(str) + 1];

	for (int i = 0; i < strlen(str); i++)
	{
		shifted = (int)str[i] + _shift;
		if (shifted > 126)
		{
			temp[i] = shifted % 94;
		}
		else
		{
			temp[i] = shifted;
		}
	}

	temp[strlen(str)] = 0;
	
	return temp;
}

bool OutStreamEncrypted::check_str(const char* str)
{
	for (int i = 0; i < strlen(str); i++)
	{
		if (str[i] < 32 || str[i] > 126)
		{
			return false;
		}
	}
	return true;
}

char * OutStreamEncrypted::int_to_string(int num)
{
	int count = 0, tNum = num;
	char* temp = nullptr;

	while (tNum > 0)
	{
		count++;
		tNum /= 10;
	}

	temp = new char[count + 1];

	for (int i = count - 1; i >= 0; i--)
	{
		temp[i] = num % 10 + '0';
		num /= 10;
	}

	temp[count] = 0;
	return temp;
}

OutStreamEncrypted::OutStreamEncrypted(int shift) : _shift(shift)
{
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(char * str)
{
	str = encrypt(str);
	this->OutStream::operator<<(str);
	return *this;
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(int num)
{
	char* temp = nullptr;
	char* str = nullptr;
	temp = int_to_string(num);
	str = encrypt(temp);
	this->OutStream::operator<<(str);
	return *this;
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(void(*pf)(FILE* f))
{
	pf(stdout);
	return *this;
}

