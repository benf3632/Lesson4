#pragma once
#include "OutStream.h"
#include <string.h>

class OutStreamEncrypted : public OutStream {
private:
	int _shift;
	char* encrypt(char* str);
	bool OutStreamEncrypted::check_str(const char* str);
	char* int_to_string(int num);
public:
	OutStreamEncrypted(int shift);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(char* str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE* f));
};